package com.axmedbek.petclinic.service;

import java.util.List;

import com.axmedbek.petclinic.exception.OwnerNotFoundException;
import com.axmedbek.petclinic.model.Owner;

public interface PetClinicService {
	List<Owner> findOwners();

	List<Owner> findOwners(String lastName);

	Owner findOwner(Long id) throws OwnerNotFoundException;

	void createOwner(Owner owner);

	void update(Owner owner);

	void deleteOwner(Long id);
}
