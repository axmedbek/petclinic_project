package com.axmedbek.petclinic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.axmedbek.petclinic.dao.OwnerRepository;
import com.axmedbek.petclinic.exception.OwnerNotFoundException;
import com.axmedbek.petclinic.model.Owner;
import com.axmedbek.petclinic.service.PetClinicService;

@Service
public class PetClinicServiceImpl implements PetClinicService {

	private OwnerRepository ownerRepository;

	@Autowired
	@Qualifier("ownerRepositoryJdbcImpl")
	public void setOwnerRepository(OwnerRepository ownerRepository) {
		this.ownerRepository = ownerRepository;
	}

	@Override
	public List<Owner> findOwners() {
		return ownerRepository.findAll();
	}

	@Override
	public List<Owner> findOwners(String lastName) {
		return ownerRepository.findByLastName(lastName);
	}

	@Override
	public Owner findOwner(Long id) throws OwnerNotFoundException {
		Owner owner = ownerRepository.findById(id);
		if (owner == null)
			throw new OwnerNotFoundException("OWner not found with id : " + id);
		return owner;
	}

	@Override
	public void createOwner(Owner owner) {
		ownerRepository.create(owner);
	}

	@Override
	public void update(Owner owner) {
		ownerRepository.update(owner);
	}

	@Override
	public void deleteOwner(Long id) {
		ownerRepository.delete(id);
	}

}
