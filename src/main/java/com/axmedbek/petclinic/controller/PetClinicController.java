package com.axmedbek.petclinic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.axmedbek.petclinic.service.PetClinicService;

@Controller
public class PetClinicController {

	@Autowired
	private PetClinicService petClinicService;

	@RequestMapping("/owners")
	public ModelAndView getOwners() {
		ModelAndView mw = new ModelAndView();
		mw.addObject("owners", petClinicService.findOwners());
		mw.setViewName("owners");
		return mw;
	}
}
