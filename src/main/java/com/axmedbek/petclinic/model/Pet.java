package com.axmedbek.petclinic.model;

import java.util.Date;

public class Pet {
	private Long id;
	private String name;
	private Date birthDate;
	private Owner owner;

	public Pet() {
	}

	public Pet(Long id, String name, Date birthDate, Owner owner) {
		this.id = id;
		this.name = name;
		this.birthDate = birthDate;
		this.owner = owner;
	}

	public Pet(String name, Date birthDate, Owner owner) {
		this.name = name;
		this.birthDate = birthDate;
		this.owner = owner;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

}
