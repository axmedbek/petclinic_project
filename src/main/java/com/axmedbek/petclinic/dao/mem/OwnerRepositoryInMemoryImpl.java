package com.axmedbek.petclinic.dao.mem;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.axmedbek.petclinic.dao.OwnerRepository;
import com.axmedbek.petclinic.model.Owner;

@Repository("ownerRepositoryImpl")
public class OwnerRepositoryInMemoryImpl implements OwnerRepository {

	private Map<Long, Owner> ownersMap = new HashMap<>();

	public OwnerRepositoryInMemoryImpl() {
		Owner ow1 = new Owner(1L, "Ahmad", "Mammadli");
		Owner ow2 = new Owner(2L, "Emin", "Xalilov");
		Owner ow3 = new Owner(3L, "Azad", "Samedov");
		Owner ow4 = new Owner(4L, "Dimitri", "Nazarov");

		ownersMap.put(ow1.getId(), ow1);
		ownersMap.put(ow2.getId(), ow2);
		ownersMap.put(ow3.getId(), ow3);
		ownersMap.put(ow4.getId(), ow4);

	}

	@Override
	public List<Owner> findAll() {
		return new ArrayList<>(ownersMap.values());
	}

	@Override
	public Owner findById(Long id) {
		return ownersMap.get(id);
	}

	@Override
	public List<Owner> findByLastName(String lastName) {
		return ownersMap.values().stream().filter(o -> o.getLastName().equals(lastName)).collect(Collectors.toList());
	}

	@Override
	public void create(Owner owner) {
		owner.setId(new Date().getTime());
		ownersMap.put(owner.getId(), owner);
	}

	@Override
	public Owner update(Owner owner) {
		ownersMap.replace(owner.getId(), owner);
		return owner;
	}

	@Override
	public void delete(Long id) {
		ownersMap.remove(id);
	}

}
