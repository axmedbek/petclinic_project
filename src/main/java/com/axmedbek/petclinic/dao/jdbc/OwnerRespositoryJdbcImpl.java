package com.axmedbek.petclinic.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.axmedbek.petclinic.dao.OwnerRepository;
import com.axmedbek.petclinic.model.Owner;

@Repository("ownerRepositoryJdbcImpl")
public class OwnerRespositoryJdbcImpl implements OwnerRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private RowMapper<Owner> rowMapper = new RowMapper<Owner>() {

		@Override
		public Owner mapRow(ResultSet rs, int rowNum) throws SQLException {
			Owner owner = new Owner();
			owner.setId(rs.getLong("id"));
			owner.setFirstName(rs.getString("first_name"));
			owner.setLastName(rs.getString("last_name"));
			return owner;
		}
	};
	@Override
	public List<Owner> findAll() {
		String sql = "select * from owners";
		return jdbcTemplate.query(sql,rowMapper);
	}

	@Override
	public Owner findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Owner> findByLastName(String lastName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void create(Owner owner) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Owner update(Owner owner) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

}
